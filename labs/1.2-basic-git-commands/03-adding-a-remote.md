# Adding a Remote

First, create a new repository in GitLab. In the top, click on the "+" icon and choose to create a new repository:

![New repository in GitLab](../../img/gitlab-new-repo.png)
 
- Select "Create blank project"
- Give the repository a name
- Optionally add a description
- Set the "Visibility Level" to "Internal" or "Public" to allow others to interact
- Uncheck "Initialize repository with README" to start with an empty project

![Creating a repository in GitLab](../../img/gitlab-new-repo-2.png)
 
The next screen will tell you how to "Push an existing Git repository", which is what we have locally.

First, add a remote:

![Adding a remote](../../img/git-remote.png)

To verify, you can list your remotes:
 
![Listing remotes](../../img/git-remotes.png)

We will continue with the "Push" on the next lab.