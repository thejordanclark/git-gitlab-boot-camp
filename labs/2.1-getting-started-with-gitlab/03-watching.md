# Watching Repositories

- Locate other's repositories though "Menu" -> "Projects" -> "Explore Projects".  Select "Watch" on the repositories notification bell.

![Project Subscribe](../../img/gitlab-watch.png)

- Create a new issue in the repository, try to @mention someone. "Issues" -> "List" -> "New Issue"

![Project Issue](../../img/gitlab-project-issue.png)

- See how the updates appear in your "Activity" feed
