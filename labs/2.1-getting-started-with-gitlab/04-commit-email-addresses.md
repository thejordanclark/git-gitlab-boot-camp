# Commit Email Addresses

To show or hide your personal email address, edit your profile:

![Settings](../../img/git-ssh-5.png)

Set your preference:

![Emails](../../img/gitlab-settings-email.png)

- Verify that Git to use this or your email address as the "commit email":

