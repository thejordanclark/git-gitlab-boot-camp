# Fetching from a Remote

Currently, we are in sync with our remote. You can verify by checking the status:

![Git status](../../img/git-fetch-status.png)

Let's change something in the remote, but not in our local repository. This simulates a change by someone else.
Go to your repository on GitLab and select a file for instance the README.md file.  Then use either the simple "Edit" ore the more powerful "Web IDE" to modify the file.

![GitLab repository](../../img/gitlab-file.png)
  
- Edit the file by adding some text:
 
- Add a commit message and commit the change to the "master" branch:

Now fetch these changes, without merging them into your working directory:

![Git fetch](../../img/git-fetch.png)

If you check the status now, you will see we're one commit behind:

![Git status](../../img/git-fetch-status-2.png)
 
Check the file on your machine to verify the change isn't in your working directory yet:

![Vim](../../img/git-fetch-cat.png)
