# Organization Accounts

Create a new group:

![Create group](../../img/gitlab-create-group.png)

- Select "Create group"
- Give the group a unique name (You may want to include a unique identifier for your group)
- Set the Group to "Internal" or "Public"
- Select a "Role"
- Set who will be using the group to "My company or team"

Invite members to your group from the "Group" -> "Members" section.

![Group members](../../img/gitlab-group-members.png)

- Invite some members
- Select a group role for the new member

![Add group member](../../img/gitlab-group-add-member.png)


When you create a new project, you can now set the group for the project location:

![Create repository](../../img/gitlab-group-new-project.png)

As an owner of the group, you can modify group Settings:

![Collaborators & teams](../../img/gitlab-group-settings.png)

- Explore the Settings available for the group