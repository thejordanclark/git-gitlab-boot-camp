# GitLab Pages

We will use a "GitLab" template to create a GitLab Pages site.  Create a new project.

![GitLab Pages](../../img/gitlab-new-repo.png)

This time select Create from template:

![GitLab Pages](../../img/gitlab-create-from-template.png)

From the templates list use "Pages/Plain HTML":

![GitLab Pages](../../img/gitlab-template-pages-html.png)

Give the project a name and optionally a description.  The "Project slug" will be part of the default site url in addition to your username or the group name the project is in.  A GitLab Pages project can also be private. 

![GitLab Pages](../../img/gitlab-new-pages-project.png)

Now that the project / repository is setup we need to manually trigger the first CI/CD Pipeline.  Any new commits will automatically trigger a new pipeline run.  The pipeline is configure in the ".gitlab-ci.yml" file.  Let's first look at that:

![GitLab Pages](../../img/gitlab-pages-gitlab-ci.png)

Notice there is one "stage" that deploys the simple HTML site.

Let's run the pipeline.  Go to "CI/CD" -> "Pipelines" and select "Run pipeline":

![GitLab Pages](../../img/gitlab-pages-run-pipeline.png)

If need be you can select the branch or add variables for the pipeline run.  Click on the "Run pipeline" button to trigger the run:

![GitLab Pages](../../img/gitlab-pages-trigger-pipeline.png)

Here you will see the pipeline running.  You can see the output by clicking on the stages.  Once the pipeline has finished we can verify the GitLab Pages status in the "Settings" -> "Pages" page for the project:

![GitLab Pages](../../img/gitlab-page-settings.png)

Go to the pages link in the "Access pages" section. You may have to accept the no trusted SSL certificate depending on how the GitLab instance is configured.  Once connected you will be presented with a website hosted via GitLab Pages.

- Make changes to the "public/index.html" file in the git repository and see how a pipeline is auto run when the commit is added.  The page is automatically updated.
