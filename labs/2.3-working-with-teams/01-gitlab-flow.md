# GitLab Flow (Merge Request)

Go to the repository from Part 1 and create a new branch:

![Create branch](../../img/gitlabflow-1.png)

Make some changes to files and add some commits. You should see additional log on the branch:

![Log](../../img/gitlabflow-2.png)

Push this branch to the remote:

![Push](../../img/gitlabflow-3.png)

In the repository on GitLab, A shortcut "Create merge request" button shows up when a new branch is pushed. If you select that button it will take you directly to the "New merge request" page pre-selected to merge your new branch into the master branch.  We will instead create a merge request from scratch through GitLab.

Select the "New merge request" button found on the projects "Merge requests" page:

![New pull request](../../img/gitlabflow-4.png)

Select the "Source branch" and the "Target branch" for the new merge request.  Commits are merged from the "Source branch" to the "Target branch".  Once selected click on the "Compare branches and continue" button.

![Create pull request](../../img/gitlabflow-5a.png)

![Create pull request](../../img/gitlabflow-5b.png)

On the resulting page, add a descriptive message.  Notice the "Merge options" section at the bottom.. Here you can select to keep the source branch or not and your fast forwarding options for the merge.  When finished select the "Create merge request" button.

![Pull request details](../../img/gitlabflow-6.png)

You will be taken to the merge request page:

![Pull request](../../img/gitlabflow-7.png)

Here you can discuss the merge request with other people that have access to the repository.

Any updates you make to the branch will be reflected in this merge request.
When you're done, merge the branch into master either on GitLab by clicking the "Merge" button (whether you fast-forward or not is up to you or your team):

![Merge](../../img/gitlabflow-8a.png)

Now you can pull the "master" branch to your local repository.

---

With out Gitlab a local merge request would look like this:

_note: we do not need to do this as we used GitLab to handle the merge._

![Merge](../../img/gitlabflow-8.png)

![Merged pull request](../../img/gitlabflow-9.png)
