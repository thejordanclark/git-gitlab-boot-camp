# Following Users

- Find each other's user accounts by using the search bar

![Search User](../../img/gitlab-user-search.png)

- Click the Follow button

![Follow User](../../img/gitlab-user-follow.png)

- Give some repositories a star or create new repositories
- See how the updates appear in your "Menu" -> "Activity" feed under "Followed Users"
