# Registering at GitLab

Go to [https://gitlab.com/users/sign_in](https://gitlab.com/users/sign_in) and follow the steps to sign in.:

![Joining GitLab](../../img/join-gitlab.png)

You have a number of options to sign in with or to create a new GitLab account.

If you want to register a new account via an email address go to [https://gitlab.com/users/sign_up](https://gitlab.com/users/sign_up) and create an account.

You have a number of options to sign in with ore to create a new account.