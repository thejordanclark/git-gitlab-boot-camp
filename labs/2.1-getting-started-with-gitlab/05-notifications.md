# Notifications

Under your profile, choose the Notifications tab to see _how_ you can configure notifications:

![GitLab notifications](../../img/gitlab-notifications.png)

- Notice the "Global notification level" settings
- You also can "Receive notifications about your own activity"
- Notifications can be set for each group and project

![GitLab notifications](../../img/gitlab-notifications-2.png)

