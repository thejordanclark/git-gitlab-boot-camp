# Issues

Follow these steps to get a feel for GitLab issues:

- Create an issue
    - Go to your Project/Repository in GitLab
    - Click the Issues tab
    - Click the "New Issue" button
    - Fill in the details

- Inside the issue you can reference
    - another Issue: #123
    - a merge request: !123
    - a snippet: :123
    - a user or group in the project: @

- Create a second issue
    - Reference the first issue by using the `#` character
    - Reference the commit by pasting the hash that was copied to your clipboard
